import React from "react";
import { createRoot } from "react-dom/client";
import { BrowserRouter as Router } from 'react-router-dom';  // new

import App from './App.jsx';

const container = document.getElementById("root");
const root = createRoot(container);

root.render((
  // new
  <Router>
    <App />
  </Router>
));